# Install Genuine Ubuntu(Desktop/Server) on Orange PI 5 Plus on NVME SSD

## Why?

Who knows what's inside of the Chinese linux build. Hello Uncle Liao ;-)

## Prequisites
- SD card (min 8 Gig)
- Installed SSD/NVME in PCIe slot

## How to (GUI version)

1. Download needed linux distro image for your chip from [https://github.com/Joshua-Riek/ubuntu-rockchip/releases](https://github.com/Joshua-Riek/ubuntu-rockchip/releases)
2. Flash image on an SD card (I use[etcher.balena](https://etcher.balena.io/))
3. Insert the card into the OrangePI and power on (insert power cable or press the power button for 2-3 seconds)
4. First boot might take up to 2-4 minutes. Be patien!
5. Walk thru the setup wizzard but don't pay much attention to it because these changes will be lost
6. Once Ubuntu has started open Chromium, navigate to the GitHub from step 1 and download the same image
7. On successfull download:
- OR Navigate to Downloads using File Managager and open the image file. Disk manager will open asking to select the drive(usually it's **nvme0n1**) for Disk Image Restoration. Agree on deletion of all data and dialog saying that image is bigger that disk(this is ok)
- OR Open Disk Manager, select the **nvme0n1**, Right mouse click on the empty partition, select Restore Disk Image. Select the image file. Agree on deletion of all data and dialog saying that image is bigger that disk(this is ok)
8. Wait until process is over and shut down ubuntu
9. Remove SD card
10. Start OragePI again (press the power button for 2-3 seconds)
11. Complete the setup wizzard once again. The partition will be expanded for the whole drive.
12. Enjoy!

## How to (Terminal version)

TBD
