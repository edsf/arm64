# Arm64 Playground

# [OrangePI 5 Plus](orangepi-5-plus)

## Support the Project

There are a few things you can do to support the project:

* Star the repository and follow me on GitLab
* Share and upvote on sites like Twitter, Reddit, and YouTube
* Report any bugs, glitches, or errors that you find (some bugs I may not be able to fix)

These things motivate me to continue development and provide validation that my work is appreciated. Thanks in advance!
